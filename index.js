#!/usr/bin/env node
'use strict'
const util = require('util')
const fs = require('fs')
const config = require('config')
const debug = require('debug')('npm-readme-viewer')
const npm = require('npm-get-prefix')
const marked = require('marked')
const TerminalRenderer = require('marked-terminal')

const stat = util.promisify(fs.stat)

marked.setOptions({
  renderer: new TerminalRenderer()
})

const pArgv = process.argv
const packageName = pArgv.length <= 2 ? pArgv[1].split('/').slice(-2,-1)[0] : pArgv[2]
const mdFileName = config.get('mdFileName')

const readFile = async (fullPath) => {
  try {
    let fileStats = await stat(fullPath)
    if (fileStats.isFile()) {
      console.log(marked(fs.readFileSync(fullPath).toString()))
    }
    return fileStats.isFile()
  } catch (e) {
    debug('An error occured while reading file: ', e)
    return false
  }
}

const exec = async () => {
  try {
      const globalPrefix = await npm.getPrefix()

      const paths = require.main.paths
          .map(path => `${path}/${packageName}/${mdFileName}`)
          .concat([`${__dirname}/${mdFileName}`])
          .concat([`${globalPrefix}/${packageName}/${mdFileName}`])
      
      let res = await Promise.all(paths.map(path => readFile(path)))
      if (res.every(res => !res)) {
        console.log(`Can't find readme file`)
      }
  } catch (e) {
    console.log(e)
  }
}

exec()
